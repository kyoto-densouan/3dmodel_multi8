# README #

1/3スケールの三菱 MULTI8風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- 三菱電機

## 発売時期
- MULTI8 1983年

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/MULTI8)
- [懐かしのホビーパソコン紹介](https://twitter.com/i/events/784693355959549952)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_multi8/raw/88b0c137be29dd649ad9e962b340e359efd5c5cc/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_multi8/raw/88b0c137be29dd649ad9e962b340e359efd5c5cc/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_multi8/raw/88b0c137be29dd649ad9e962b340e359efd5c5cc/ExampleImage.png)
